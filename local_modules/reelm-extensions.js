import { spoiled } from 'reelm';

export function perform(fn) {
    return (state, action) => spoiled(state, function * () {
        yield * fn(action);
    });
}
