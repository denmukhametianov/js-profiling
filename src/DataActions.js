import {dispatch} from './Dispatcher';

function changeItem(row, col, value) {
    dispatch({
        type: 'change',
        row,
        col,
        value,
    })
}


export default {
    changeItem,
};
