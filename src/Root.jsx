import React from 'react';
import ContentBlock from '../containers/ContentBlock/ContentBlock';
import Header from '../containers/Header/Header.jsx';
import {Container} from 'flux/utils';
import Immutable from 'immutable';
import HugeTable from './HugeTable';

import DataStore from './DataStore';
import DataActions from './DataActions';

class Root extends React.Component {
    render() {
        const {data} = this.props;
        return <div>
            <Header>Header</Header>
            <ContentBlock>
                <ContentBlock.Body>
                    <ContentBlock.Title>
                        Huge table
                    </ContentBlock.Title>
                    <ContentBlock.Value>
                        <HugeTable data={data} onChange={(row, col, value) => DataActions.changeItem(row, col, value)}/>
                    </ContentBlock.Value>
                </ContentBlock.Body>
            </ContentBlock>
        </div>;
    }
}

class RootContainer extends React.Component {
    static getStores() {
        return [DataStore];
    }

    static calculateState(prevState, props) {
        const st = DataStore.getState();
        const data = st.get('data');
        return {data};
    }

    render() {
        return <Root {...this.state}/>;
    }
}

export default Container.create(RootContainer);
