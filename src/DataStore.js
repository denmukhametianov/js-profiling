import dispatcher from './Dispatcher';
import {ReduceStore} from 'flux/utils';
import {List, Map} from 'immutable';

class DataStore extends ReduceStore {
    getInitialState() {
        const rows = 1000;
        const cols = 9;
        var data = List();
        for (let row = 0; row < rows; row++) {
            data = data.set(row, List());
            for (let col = 0; col < cols; col++) {
                data = data.update(row, x => x.push(row + col));
            }
        }
        return Map({data: data});
    }

    reduce(state, action) {
        switch (action.type) {
            case 'change':
                return state.setIn(['data', action.row, action.col], action.value);
        }
        return state;
    }
}

export default new DataStore(dispatcher);
