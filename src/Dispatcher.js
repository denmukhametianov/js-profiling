import {Dispatcher} from 'flux';

const dispatcher = new Dispatcher();

export default dispatcher;

export function dispatch(...args) {
    dispatcher.dispatch(...args)
}
