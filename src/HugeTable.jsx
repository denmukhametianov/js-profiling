import React from 'react';
import {Input} from 'ui';
import Immutable from 'immutable';

class Row extends React.Component {
    render() {
        const {row, rown, onChange, hasError} = this.props;
        return <tr key={rown}>
            {row.map((value, coln) => <td key={coln}>
                <Input error={hasError} width={100} value={value} onChange={(e, v) => onChange(rown, coln, v)}/>
            </td>)}
        </tr>;
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.row !== this.props.row;
    }
}

function memoize(self, name, descriptor) {
    let action = descriptor.value;
    return {
        ...descriptor,
        value(...args) {
            if(this[args] === undefined) {
                this[args] = action.call(this, ...args);
            }
            return this[args];
        }
    }
}

export default class HugeTable extends React.Component {
    render() {
        const {data, onChange} = this.props;
        return (<table>
            <tbody>
            {data && data.map((row, rowIdx) => <Row key={rowIdx}
                                                    onChange={onChange}
                                                    row={row}
                                                    rown={rowIdx}
                                                    hasError={this._hasError(row)}
            />)}
            </tbody>
        </table>);
    }

    @memoize
    _hasError(row) {
        var result = 0;
        for(let i = 0; i < 1000000; i++) {
            result += i;
        }
       return row.some((value) => value === '');
    }
}
