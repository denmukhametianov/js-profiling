import React from 'react';
import ContentBlock from '../ContentBlock/ContentBlock.jsx';
import styles from './BigButton.less';

export default function BigButton(props) {
    return <ContentBlock>
        <div className={styles.bigButton} onClick={() => props.onClick()}>{props.children}</div>
    </ContentBlock>;
}
