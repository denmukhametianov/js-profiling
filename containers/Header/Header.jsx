import React from 'react';
import {Link, Icon, Input} from 'ui';
import Logo from './Logo/Logo';
import styles from './Header.less';

export default function Header(props) {
    return <div {...props} className={styles.header}>
        <div>
            <Logo className={styles.logo} color="rgb(211,49,18)"/>
        </div>
        <div  className={styles.title}>Развлечения</div>
    </div>;
}

