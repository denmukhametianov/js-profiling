import React from 'react';
import styles from './ContentBlock.less';

export default function ContentBlock(props) {
    return <div className={styles.root}>
        {props.children}
    </div>
}

import Title from './Title/Title';
import Label from './Label/Label';
import Value from './Value/Value';
import BottomPanel from './BottomPanel/BottomPanel';
import Body from './Body/Body';

ContentBlock.Title = Title;
ContentBlock.Label = Label;
ContentBlock.Value = Value;
ContentBlock.BottomPanel = BottomPanel;
ContentBlock.Body = Body;
