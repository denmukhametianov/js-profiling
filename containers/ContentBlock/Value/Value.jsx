import React from 'react';
import styles from '../ContentBlock.less';

export default function Value(props) {
    return <span className={styles.value}>{props.children ? props.children : <span className={styles.empty}>не указано</span>}</span>
}
