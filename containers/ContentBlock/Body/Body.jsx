import React from 'react';
import styles from '../ContentBlock.less';
import isArray from 'isArray';

import {Gapped} from 'ui';

export default function Body(props) {
    let actions = null;
    if (props.actions) {
        actions = props.actions();
    }

    return <div className={styles.body}>
        {actions && <div className={styles.actions}>
            <Gapped gap={18}>{isArray(actions) ? actions.map((x, idx)=><span key={idx}>{x}</span>) : actions}</Gapped>
        </div>}
        {props.children}
    </div>;
}
